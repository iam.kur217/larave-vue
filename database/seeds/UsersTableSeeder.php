<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Keyur Vamja',
            'email' => 'keyur@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
